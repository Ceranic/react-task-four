import React from 'react'
import { withRouter } from 'react-router-dom'
import './ListItem.css'

function ListItem(props) {

    const openDetails = () => {
        props.history.push("/flightDetails/"+props.id);
    }


    return (
        <div className="planeListItem" onClick={openDetails}>
            <div className="planeId">{props.id +1}</div>
            <div className="planeCompanyName">{props.plane.company}</div>
            <div className="planeWebsite">{props.plane.website}</div>
            <div className="planeDate">{props.plane.date}</div>
        </div>
    )
}

export default withRouter(ListItem)
