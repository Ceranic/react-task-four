import React from 'react';
import './HomeComponent.css';
import ListItem from './ListItem';

function HomeComponent(props) {


    return (
        <div>
            {props.airplaneData.map((plane,id) => <ListItem key={id} id={id} plane={plane} />)}
        </div>
    )
}

export default HomeComponent
