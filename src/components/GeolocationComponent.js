import React from 'react'
import './GeolocationComponent.css'

function GeolocationComponent(props) {
    return (
        <div className="Geolocation">
            <h1 className="geoHeader">Geolocation</h1>
            <div className="geoAbout">
                {props.animationState ? <div className="AnimationCircle"></div> : props.message}
            </div>
            <button onClick={props.requestLocation}>Allow GeoLocation</button>
        </div>
    )
}

export default GeolocationComponent;