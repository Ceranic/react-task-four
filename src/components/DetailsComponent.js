import React from 'react'
import {withRouter, Link} from 'react-router-dom'
import './DetailsComponent.css';

function DetailsComponent(props) {
    
    const id = Number(props.match.params.id);
    const currentAirplane = props.airplaneData[id];

    console.log(currentAirplane);
    
    return currentAirplane ? (
    <div className="detailsMain">
        <div className="detailsImgDiv">
            <Link className="detailsBack" to="/">Back</Link>
            <div className="detailsImg">
                <div className="detailsNoImg">No image</div>
                <img src={'http://logo.clearbit.com/' + currentAirplane.website}></img>
            </div>
        </div>
        <div className="detailsItem">
            <div className="detailsName">Company name:</div>
            <div className="detailsValue">{currentAirplane.company}</div>
        </div>
        <div className="detailsItem">
            <div className="detailsName">Website:</div>
            <div className="detailsValue">{currentAirplane.website}</div>
        </div>
        <div className="detailsItem">
            <div className="detailsName">Email:</div>
            <div className="detailsValue">{currentAirplane.email}</div>
        </div>
        <div className="detailsItem">
            <div className="detailsName">Date:</div>
            <div className="detailsValue">{currentAirplane.date}</div>
        </div>
        <div className="detailsItem">
            <div className="detailsName">Phone:</div>
            <div className="detailsValue">{currentAirplane.phone}</div>
        </div>
        {currentAirplane.props.map((x,y) => (
         <div className="detailsItem" key={y}>
            <div className="detailsName">{x.prop}</div>
            <div className="detailsValue">{x.value}</div>
        </div>
        ))}
    </div>
    ) : (<></>)

}

export default withRouter(DetailsComponent);