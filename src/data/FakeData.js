const FakeData = [{
    company: "Qatar Airways",
    website: "qatarairways.com",
    email: "support@qatarairways.com",
    date: "2019-11-08",
    phone: "+(468) 839-0288",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Singapore Airlines",
    website: "singaporeair.com",
    email: "support@singaporeair.com",
    date: "2019-11-11",
    phone: "+(206) 535-3742",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "ANA All Nippon Airways",
    website: "ana.co.jp",
    email: "support@ana.co.jp",
    date: "2019-11-12",
    phone: "+(846) 980-3368",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Cathay Pacific Airways",
    website: "cathaypacific.com",
    email: "support@cathaypacific.com",
    date: "2019-11-18",
    phone: "+(484) 452-5821",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Emirates",
    website: "emirates.com",
    email: "support@emirates.com",
    date: "2019-11-20",
    phone: "+(617) 873-9107",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "EVA Air",
    website: "evaair.com",
    email: "support@evaair.com",
    date: "2019-11-25",
    phone: "+(335) 845-4820",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Hainan Airlines",
    website: "hainanairlines.com",
    email: "support@hainanairlines.com",
    date: "2019-11-26",
    phone: "+(709) 272-6750",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Qantas Airways",
    website: "qantas.com",
    email: "support@qantas.com",
    date: "2019-12-04",
    phone: "+(376) 913-6479",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Lufthansa",
    website: "lufthansa.com",
    email: "support@lufthansa.com",
    date: "2019-12-09",
    phone: "+(523) 351-5246",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Thai Airways",
    website: "thaiairways.com",
    email: "support@thaiairways.com",
    date: "2019-12-10",
    phone: "+(713) 220-9562",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Air Serbia",
    website: "airserbia.com",
    email: "support@airserbia.com",
    date: "2019-12-13",
    phone: "+(238) 813-0659",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Alaska Airlines",
    website: "alaskaair.com",
    email: "support@alaskaair.com",
    date: "2019-12-16",
    phone: "+(212) 729-6448",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Delta Airlines",
    website: "delta.com",
    email: "support@delta.com",
    date: "2019-12-19",
    phone: "+(483) 255-1171",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Spirit Airlines",
    website: "spirit.com",
    email: "support@spirit.com",
    date: "2019-12-20",
    phone: "+(659) 343-0591",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "jetBlue",
    website: "jetblue.com",
    email: "support@jetblue.com",
    date: "2019-12-31",
    phone: "+(495) 482-3717",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "IndiGo",
    website: "goindigo.in",
    email: "support@goindigo.in",
    date: "2019-01-03",
    phone: "+(969) 266-2301",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "SpiceJet",
    website: "spicejet.com",
    email: "support@spicejet.com",
    date: "2019-09-06",
    phone: "+(790) 444-9812",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "GoAir",
    website: "goair.in",
    email: "support@goair.in",
    date: "2019-10-04",
    phone: "+(917) 460-0358",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "AirAsia",
    website: "airasia.com",
    email: "support@airasia.com",
    date: "2019-11-18",
    phone: "+(630) 550-3773",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
},{
    company: "Vistara",
    website: "airvistara.com",
    email: "support@airvistara.com",
    date: "2019-11-20",
    phone: "+(442) 684-4823",
    props: [
        {prop: "Airplanes", value: 32},
        {prop: "Customers", value: 215},
        {prop: "Average travel time (hours)", value: 12.5},
        {prop: "Average airplane speed", value: "800mph"},
    ]
}];

export default FakeData;