import React, {useState} from 'react';
import {BrowserRouter,Switch,Route, Redirect} from 'react-router-dom'
import './App.css';
import axios from 'axios';
import FakeData from './data/FakeData'


//Importing components
import HomeComponent from './components/HomeComponent';
import DetailsComponent from './components/DetailsComponent';
import GeolocationComponent from './components/GeolocationComponent'


function App() {

  const [message, setMessage] = useState('Please allow geolocation to continue');

  const [animationState, setAnimationState] = useState(false);
  const [airplaneData, setAirplaneData] = useState([]);
  const [geolocated, setGeolocated] = useState(false);

  const requestLocation = () => {
    
    setAnimationState(true);
    if (navigator.geolocation) { 
      navigator.geolocation.getCurrentPosition((response) => {

        const {latitude, longitude} = response.coords;
        const url = `https://adsbexchange.com/api/aircraft/json/lat/${latitude}/lon/${longitude}/dist/10/`
        
        axios({
          url: url,
          method: 'GET'
        }).then((res) => {
          setAnimationState(false);
          setAirplaneData(FakeData);
          setGeolocated(true);
        })
      
      
      },(error) => {
        setAnimationState(false);
        setMessage(error.message);
      });
    } else {
      setAnimationState(false); 
      setMessage("Oops! This browser does not support HTML Geolocation.");
    }
  }

  return (
    <BrowserRouter className="App">
      <Switch>
        <Route exact path="/" render={() => <HomeComponent airplaneData={airplaneData}/>}/>
        <Route exact path="/flightDetails/:id" render={() => <DetailsComponent airplaneData={airplaneData}/>}/>
        <Redirect to="/"/>
      </Switch>
      {geolocated ? "" : <GeolocationComponent animationState={animationState} message={message} requestLocation={requestLocation} />}
    </BrowserRouter>
  );
}

export default App;
